package com.example.font.fontcheck.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ListPopupWindow;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.example.font.fontcheck.R;
import com.example.font.fontcheck.item.FontItem;
import com.example.font.fontcheck.utils.FontCache;
import com.example.font.fontcheck.utils.TextWatcherAdapter;
import com.example.font.fontcheck.utils.ViewUtils;
import com.example.font.fontcheck.widget.FontPopup;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private EditText firstInput;
    private EditText secondInput;
    private TextView firstTxt;
    private TextView secondTxt;

    private ListPopupWindow popupWindow;
    private List<FontItem> listA, listB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fillList();

        findViewById(R.id.a_btn).setOnClickListener(this);
        findViewById(R.id.b_btn).setOnClickListener(this);

        firstInput = (EditText) findViewById(R.id.first_input);
        secondInput = (EditText) findViewById(R.id.second_input);

        firstTxt = (TextView) findViewById(R.id.first_txt);
        secondTxt = (TextView) findViewById(R.id.second_txt);

        ViewUtils.viewSizeInflated(firstTxt, new Runnable() {
            @Override
            public void run() {
                setParams(firstTxt);
            }
        });
        ViewUtils.viewSizeInflated(secondTxt, new Runnable() {
            @Override
            public void run() {
                setParams(secondTxt);
            }
        });

        firstInput.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                firstTxt.setText(charSequence.toString());
            }
        });

        secondInput.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                secondTxt.setText(charSequence.toString());
            }
        });
    }


    @Override
    public void onClick(View view) {
        if (popupWindow != null) { popupWindow.dismiss(); }
        switch (view.getId()) {
            case R.id.a_btn:
                popupWindow = FontPopup.show(view, listA, new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        setFont(listA.get(i));
                        popupWindow.dismiss();
                    }
                });
                break;
            case R.id.b_btn:
                popupWindow = FontPopup.show(view, listB, new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        setFont(listB.get(i));
                        popupWindow.dismiss();
                    }
                });
                break;
        }
    }

    private void setParams(View view) {
        RelativeLayout.LayoutParams params = (LayoutParams) view.getLayoutParams();
        params.height = view.getHeight();
        params.width = view.getWidth();
        view.setLayoutParams(params);
    }

    private void fillList() {
        listA = new ArrayList<>();
        listB = new ArrayList<>();
        listA.add(new FontItem(getString(R.string.font_signika), getString(R.string.signika_regular)));
        listA.add(new FontItem(getString(R.string.font_traveling), getString(R.string.traveling_typewriter)));
        listA.add(new FontItem(getString(R.string.font_asunaskh), getString(R.string.asunaskh)));

        listB.add(new FontItem(getString(R.string.font_signika), getString(R.string.signika_regular)));
        listB.add(new FontItem(getString(R.string.font_traveling), getString(R.string.traveling_typewriter)));
        listB.add(new FontItem(getString(R.string.font_jameel), getString(R.string.jameel_noori_nastaleeq)));
        listB.add(new FontItem(getString(R.string.font_alvi_nastaleeq), getString(R.string.alvi_nastaleeq)));
        listB.add(new FontItem(getString(R.string.font_asunaskh), getString(R.string.asunaskh)));
        listB.add(new FontItem(getString(R.string.font_fajer_noori), getString(R.string.fajer_noori)));
        listB.add(new FontItem(getString(R.string.font_pak_nastaleeq), getString(R.string.pak_nastaleeq)));

    }

    private void setFont(FontItem fontItem) {
        TextView focusedInputView = null, focusedTextView = null;
        boolean isArabicFont = !(fontItem.getName().equals(getString(R.string.signika_regular))
                || fontItem.getName().equals(getString(R.string.traveling_typewriter)));

        if (firstInput.hasFocus()) {
            focusedInputView = firstInput;
            focusedTextView = firstTxt;
            if (isArabicFont) {
                firstTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                     getResources().getDimension(R.dimen.first_font_size_small));
            } else {
                firstTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                     getResources().getDimension(R.dimen.first_font_size));
            }
        } else if (secondInput.hasFocus()) {
            focusedInputView = secondInput;
            focusedTextView = secondTxt;
            if (isArabicFont) {
                secondTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                      getResources().getDimension(R.dimen.second_font_size_small));
            } else {
                secondTxt.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                                      getResources().getDimension(R.dimen.second_font_size));
            }
        }

        if (focusedInputView != null) {
            FontCache.setFont(focusedInputView, fontItem.getPath());
            FontCache.setFont(focusedTextView, fontItem.getPath());


        }
    }

}
