package com.example.font.fontcheck.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.annotation.IdRes;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Util class for get and setup Fonts
 * <p>
 * Created by Wolf on 20.11.2015
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class FontCache {

    /**
     * Instance of the FontCache
     */
    private static final FontCache INSTANCE = new FontCache();

    private static final String TAG = FontCache.class.getSimpleName();
    /**
     * Directory in assets with all fonts
     */
    private static String mAssetsDirectory;
    //TODO test before commit!!!!!!!
    private static String sLastUsedFont = "";
    /**
     * Is FontCache was initialized
     */
    private volatile boolean mWasInitialized = false;
    /**
     * Is current Typeface loading from setup
     */
    private volatile boolean mFontExist = false;
    /**
     * Current application Context
     */
    private volatile Context mAppContext;
    /**
     * Cache for used Typeface
     */
    private ConcurrentMap<String, Typeface> mData;

    private FontCache() {
        //Nothing to do there
    }

    /**
     * Public initializer of FontCache
     *
     * @param context         Context of the current Activity
     * @param assetsDirectory Directory of the fonts in the assets
     * @return Current instance of the FontCache
     */
    public static synchronized FontCache init(Context context, String assetsDirectory) {
        if (context == null || TextUtils.isEmpty(assetsDirectory)) {
            throw new RuntimeException("You must provide a valid context and assets directory name when initializing FontCache");
        }

        if (!INSTANCE.mWasInitialized) {
            INSTANCE.initWithContext(context, assetsDirectory);
        }

        return INSTANCE;
    }

    /**
     * Setup all typefaces on the start of the application
     *
     * @param listener Listener for callback from setup
     */
    public static void setupFonts(final OnFontsLoadedListener listener) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                return getInstance().getListAssetFiles(mAssetsDirectory);
            }

            @Override
            protected void onPostExecute(Boolean successful) {
                getInstance().mFontExist = false;
                listener.onFontsLoaded(successful);
            }
        }.execute();
    }

    /**
     * Get Instance of the FontCache
     *
     * @return Instance
     */
    public static FontCache getInstance() {
        if (!INSTANCE.mWasInitialized) {
            throw new RuntimeException(
                    "FontCache was not initialized! You must run FontCache.init() before using this");
        }
        return INSTANCE;
    }

    /**
     * Get Typeface by name from assets
     *
     * @param fontName Name of the Typeface
     * @return requested Typeface or Default one if not exist
     */
    public static Typeface getFont(String fontName) {
        if (!getInstance().mData.containsKey(fontName)) {
            if (getInstance().mFontExist || assetExists(getInstance().mAppContext
                                                                .getAssets(), mAssetsDirectory + "/" + fontName)) {
                Typeface typeface = Typeface
                        .createFromAsset(getInstance().mAppContext.getAssets(), mAssetsDirectory + "/" + fontName);
                Log.i(TAG, fontName + " loaded");
                getInstance().mData.put(fontName, typeface);
            } else {
                Log.e(TAG, "Fonts " + fontName + " doesn't exist.");
                return Typeface.DEFAULT;
            }
        }
        return getInstance().mData.get(fontName);
    }
    //-----------------------------------------------------------------------

    //-----------------------------------------------------------------------
    public static void emptyStub() {
    }

    /**
     * Set Typeface to a view which is/extends a TextView. Uses last-used font
     *
     * @param parent parent view to search in
     * @param viewId id of target view
     */
    public static void setFont(View parent, @IdRes int viewId) {
        setFont(parent, viewId, sLastUsedFont);
    }

    /**
     * Set Typeface to a view which is/extends a TextView
     *
     * @param parent   parent view to search in
     * @param viewId   id of target view
     * @param fontName requested typeface name
     */
    public static void setFont(View parent, @IdRes int viewId, String fontName) {
        sLastUsedFont = fontName;
        if (parent == null) {
            return;
        }
        TextView view = (TextView) parent.findViewById(viewId);
        setFont(view, fontName);
    }

    /**
     * Set Typeface to TextView.
     *
     * @param target   TextView which need font changes
     * @param fontName Name for the current Typeface (stored in class Fonts)
     */
    public static void setFont(TextView target, String fontName) {
        sLastUsedFont = fontName;
        target.setTypeface(getFont(fontName));
    }

    /**
     * Return is current file exists in the assets folder
     *
     * @param assets AssetManager for current context
     * @param name   name of the file what we should check
     */
    private static boolean assetExists(AssetManager assets, String name) {

        try {
            // using File to extract path / filename
            // alternatively use name.lastIndexOf("/") to extract the path
            File f = new File(name);
            String parent = f.getParent();
            if (parent == null)
                parent = "";
            String fileName = f.getName();
            // now use path to base_explode all files
            List<String> assetList = Arrays.asList(assets.list(parent));
            if (assetList.size() > 0) {
                return assetList.contains(fileName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Private initializer of the FontCache
     *
     * @param context         Context of the current Activity
     * @param assetsDirectory Directory of the fonts in the assets
     */
    private void initWithContext(Context context, String assetsDirectory) {
        long start = SystemClock.uptimeMillis();

        if (!assetExists(context.getAssets(), assetsDirectory)) {
            throw new RuntimeException("Assets directory name doesn't exist");
        }

        mAppContext = context.getApplicationContext();
        mAssetsDirectory = assetsDirectory;

        mData = new ConcurrentHashMap<>();
        mWasInitialized = true;

        long delta = SystemClock.uptimeMillis() - start;
        Log.i(TAG, "FontCache tool " + delta + " ms to init");
    }

    /**
     * Get base_explode of the typefaces from Assets Directory
     *
     * @param path Assets directory for typefaces
     * @return Found Fonts
     */
    private boolean getListAssetFiles(String path) {
        String[] list;
        mFontExist = false;
        try {
            list = mAppContext.getAssets().list(path);
            if (list.length > 0) {
                // This is a folder
                for (String file : list) {
                    if (!getListAssetFiles(path + "/" + file))
                        return false;
                }
            } else {
                // This is a file
                mFontExist = true;
                getFont(path.substring(path.indexOf("/") + 1));
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Listener to Typefaces load on setup
     */
    public interface OnFontsLoadedListener {
        void onFontsLoaded(boolean successful);
    }
}
