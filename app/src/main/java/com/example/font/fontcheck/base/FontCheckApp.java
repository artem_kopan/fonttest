package com.example.font.fontcheck.base;

import android.app.Application;

import com.example.font.fontcheck.utils.FontCache;

/**
 * Created by Artem Kopan for FontTest
 * 15.12.16
 */

public class FontCheckApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FontCache.init(getApplicationContext(), "fonts");
    }
}
