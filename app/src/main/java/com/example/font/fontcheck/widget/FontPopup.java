package com.example.font.fontcheck.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.ListPopupWindow;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;

import com.example.font.fontcheck.item.FontItem;

import java.util.List;

/**
 * Created by Artem Kopan for FontTest
 * 15.12.16
 */

public class FontPopup {

    @SuppressWarnings("unchecked")
    public static ListPopupWindow show(@NonNull View anchor, List<FontItem> fonts,
                                       final OnItemClickListener fontSelectedListener) {

        final Context context = anchor.getContext();

        ListPopupWindow listPopupWindow = new ListPopupWindow(context);
        listPopupWindow.setAdapter(new ArrayAdapter(context,
                                                    android.R.layout.simple_dropdown_item_1line,
                                                    fonts));

        listPopupWindow.setAnchorView(anchor);
        listPopupWindow.setWidth(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setHeight(ListPopupWindow.WRAP_CONTENT);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(fontSelectedListener);
        listPopupWindow.show();


//        final View content = LayoutInflater.from(context).inflate(R.layout.layout_fonts, null);
//
//        final int decorWidth = decorView.getWidth();
//        final int decorHeight = decorView.getHeight();
//        content.measure(
//                View.MeasureSpec.makeMeasureSpec(decorWidth, View.MeasureSpec.AT_MOST),
//                View.MeasureSpec.makeMeasureSpec(decorHeight, View.MeasureSpec.AT_MOST));
//
//        final PopupWindow popupWindow = new PopupWindow(content,
//                                                        content.getMeasuredWidth(),
//                                                        content.getMeasuredHeight());
//        popupWindow.setTouchable(true);
//        popupWindow.showAsDropDown(anchor);
//
//        final OnClickListener onClickListener = new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (fontSelectedListener == null) {
//                    return;
//                }
//                switch (view.getId()) {
//                    case R.id.jameel_btn:
//                        fontSelectedListener.onFontSelected(context.getString(R.string.font_jameel));
//                        break;
//                    case R.id.signika_btn:
//                        fontSelectedListener.onFontSelected(context.getString(R.string.font_signika));
//                        break;
//                    case R.id.traveling_btn:
//                        fontSelectedListener.onFontSelected(context.getString(R.string.font_traveling));
//                        break;
//                    case R.id.alvi_nastaleeq_btn:
//                        fontSelectedListener.onFontSelected(context.getString(R.string.font_alvi_nastaleeq));
//                        break;
//                    case R.id.asunaskh_btn:
//                        fontSelectedListener.onFontSelected(context.getString(R.string.font_asunaskh));
//                        break;
//                    case R.id.fajer_noori_btn:
//                        fontSelectedListener.onFontSelected(context.getString(R.string.font_fajer_noori));
//                        break;
//                    case R.id.pak_nastaleeq_btn:
//                        fontSelectedListener.onFontSelected(context.getString(R.string.font_pak_nastaleeq));
//                        break;
//                }
//                popupWindow.dismiss();
//            }
//        };
//        content.findViewById(R.id.jameel_btn).setOnClickListener(onClickListener);
//        content.findViewById(R.id.signika_btn).setOnClickListener(onClickListener);
//        content.findViewById(R.id.traveling_btn).setOnClickListener(onClickListener);
//        content.findViewById(R.id.alvi_nastaleeq_btn).setOnClickListener(onClickListener);
//        content.findViewById(R.id.asunaskh_btn).setOnClickListener(onClickListener);
//        content.findViewById(R.id.fajer_noori_btn).setOnClickListener(onClickListener);
//        content.findViewById(R.id.pak_nastaleeq_btn).setOnClickListener(onClickListener);

        return listPopupWindow;
    }

}
