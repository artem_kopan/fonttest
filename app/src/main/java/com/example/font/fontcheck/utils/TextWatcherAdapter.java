package com.example.font.fontcheck.utils;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created for fonttest
 * by Kopan Artem on 15.12.2016.
 */

public abstract class TextWatcherAdapter implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
