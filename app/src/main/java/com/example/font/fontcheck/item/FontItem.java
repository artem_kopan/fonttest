package com.example.font.fontcheck.item;

/**
 * Created for fonttest
 * by Kopan Artem on 16.12.2016.
 */

public class FontItem {

    private String path;
    private String name;

    public FontItem(String path, String name) {
        this.path = path;
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
