package com.example.font.fontcheck.utils;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewTreeObserver.OnPreDrawListener;

/**
 * Created by Artem Kopan for FontTest
 * 16.12.16
 */

public class ViewUtils {

    /**
     * Check view size, if {@link View#getWidth()} or {@link View#getHeight()} > 0, then return true;
     */
    public static boolean checkSize(View view) {
        return view.getWidth() > 0 || view.getHeight() > 0;
    }

    /**
     * View on pre draw call
     */
    public static void preDrawListener(final View view, @NonNull final Runnable onPreDraw) {
        if (view.getViewTreeObserver().isAlive()) {
            view.getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    if (view.getViewTreeObserver().isAlive()) {
                        view.getViewTreeObserver().removeOnPreDrawListener(this);
                        onPreDraw.run();
                    }
                    return true;
                }
            });
        }
    }

    /**
     * Firstly check view size. If size (w ro h) <= 0, then call preDraw.
     */
    public static void viewSizeInflated(View view, @NonNull Runnable ready) {
        if (checkSize(view)) {
            ready.run();
        } else {
            preDrawListener(view, ready);
        }
    }

}
